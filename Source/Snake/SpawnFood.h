// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusSpeed.h"
#include "Food.h"
#include "SnakeGameModeBase.h"
#include "StopBonus.h"
#include "BonusStar.h"
#include "GameFramework/Actor.h"
#include "SpawnFood.generated.h"

class AFood;
class UHierarchicalInstancedStaticMeshComponent;



UCLASS()
class SNAKE_API ASpawnFood : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnFood();
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood>FoodClass_Apple;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood>FoodClass_Banan;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood>FoodClass_Orange;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonusSpeed>BonusSpeedClass;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonusStar>BonusStarClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AStopBonus>StopBonusClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABlockAndDestroy>BlockAndDestroyClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood>FoodClass_Time;

	UPROPERTY(EditAnywhere)
	float Padding;
	
	FTimerHandle FoodHandle;

	UFUNCTION()
	void OnTimerFood();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void GenerateFloor();

	UPROPERTY(EditAnywhere, Category= "Map Settings")
		int MapSize = 5;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TArray<FVector> ArrayLocations;

	UPROPERTY(EditDefaultsOnly)
		FVector InitVector ;

	UPROPERTY(EditDefaultsOnly)
		int x=5;
	UPROPERTY(EditDefaultsOnly)
		int y=5;
	UPROPERTY(EditDefaultsOnly)
	float Element;

	float X;
	float Y;
	float Z;

	float MapCenter;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddFoodElement(int Element=1);

	FVector GetRandomFoodPos();

	
	

};
