// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_Tank_Char.h"
#include "SnakeGameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "SnakeBase.h"

// Sets default values
AAI_Tank_Char::AAI_Tank_Char()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAI_Tank_Char::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAI_Tank_Char::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAI_Tank_Char::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AAI_Tank_Char::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			ASnakeGameModeBase* gameMode = GetWorld()->GetAuthGameMode<ASnakeGameModeBase>();
			gameMode->GetGameOverWidget()->SetVisibility(ESlateVisibility::Visible);
			auto PlayerController = GetWorld()->GetFirstPlayerController();
			FInputModeUIOnly InputModeData;
			PlayerController->SetInputMode(InputModeData);
			PlayerController->bShowMouseCursor = true;
			
		}


	}


}