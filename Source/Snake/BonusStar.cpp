// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusStar.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeGameModeBase.h"
#include"SnakeBase.h"

// Sets default values
ABonusStar::ABonusStar()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABonusStar::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonusStar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void ABonusStar::Interact(AActor* Interactor, bool bIsHead)
{

	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			ASnakeGameModeBase* gameMode = GetWorld()->GetAuthGameMode<ASnakeGameModeBase>();
			gameMode->SetBonusScore();
			Destroy();
		}
	}



}
