// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include "SnakeGameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "SnakeBase.h"


// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABullet::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			ASnakeGameModeBase* gameMode = GetWorld()->GetAuthGameMode<ASnakeGameModeBase>();
			gameMode->GetGameOverWidget()->SetVisibility(ESlateVisibility::Visible);
			auto PlayerController = GetWorld()->GetFirstPlayerController();
			FInputModeUIOnly InputModeData;
			PlayerController->SetInputMode(InputModeData);
			PlayerController->bShowMouseCursor = true;
			//Destroy();

		}


	}


}

