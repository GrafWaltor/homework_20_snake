// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnFood.h"
#include "Food.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "SnakeGameModeBase.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ASpawnFood::ASpawnFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");

}

// Called when the game starts or when spawned
void ASpawnFood::BeginPlay()
{
	GenerateFloor();
	GetRandomFoodPos();
	AddFoodElement(0);

}

void ASpawnFood::Tick(float DeltaTime)
{
Super::Tick(DeltaTime);
}

void ASpawnFood::GenerateFloor()
{
	ArrayLocations.Empty();
	FVector L_Scale = FVector(2, 2, 2);
	FVector L_Location = FVector(0,0,0);
	float ElementSize = 450.f;
	ElementSize = Element;
	
	for (int i = 1; i <= x; i++)
	{
		 X = i * Element;

		for (int j = 1; j <= y; j++)
		{
			Y = j * Element;

			L_Location = UKismetMathLibrary::MakeVector(X, Y,Z)+InitVector;

			FTransform SpawnTransform = UKismetMathLibrary::MakeTransform(
				L_Location,
				FRotator::ZeroRotator,
				L_Scale
			);

			ArrayLocations.Add(L_Location);
		}
	}
}

   FVector ASpawnFood::GetRandomFoodPos()
{
	FVector FoodLocations;
	FoodLocations = ArrayLocations[FMath::RandRange(0, ArrayLocations.Num() - 1)];
	return FoodLocations;
}

void ASpawnFood::OnTimerFood()
{

	AddFoodElement(0);
	
	
}

void ASpawnFood::AddFoodElement(int Elements)
{

	for (int i=0; i<=Elements;++i)
		
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
		FVector L_Scale = FVector(2, 2, 2);
		FVector NewLocation = GetRandomFoodPos();
		FTransform NewTransform = UKismetMathLibrary::MakeTransform(
			NewLocation,
			FRotator::ZeroRotator,
			L_Scale
		);
		GetWorld()->SpawnActor<AFood>(FoodClass_Apple,NewTransform);
		GetWorldTimerManager().SetTimer(FoodHandle,this,&ASpawnFood::OnTimerFood,1.0f, true, 1.0f);
		
	}

	
	for (int i=0; i<=Elements;++i)
		
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
		FVector L_Scale = FVector(2, 2, 2);
		FVector NewLocation = GetRandomFoodPos();
		FTransform NewTransform = UKismetMathLibrary::MakeTransform(
			NewLocation,
			FRotator::ZeroRotator,
			L_Scale
		);
		GetWorld()->SpawnActor<AFood>(FoodClass_Banan,NewTransform);

		GetWorldTimerManager().SetTimer(FoodHandle,this,&ASpawnFood::OnTimerFood,2.0f, true, 2.0f);
		

	}
	
	
	for (int i=0; i<=Elements;++i)
		
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
		FVector L_Scale = FVector(2, 2, 2);
		FVector NewLocation = GetRandomFoodPos();
		FTransform NewTransform = UKismetMathLibrary::MakeTransform(
			NewLocation,
			FRotator::ZeroRotator,
			L_Scale
		);
		GetWorld()->SpawnActor<AFood>(FoodClass_Orange,NewTransform);
		GetWorldTimerManager().SetTimer(FoodHandle,this,&ASpawnFood::OnTimerFood,3.0f, true, 3.0f);
	}
	
	
	for (int i=0; i<=Elements;++i)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
		FVector L_Scale = FVector(2, 2, 2);
		FVector NewLocation = GetRandomFoodPos();
		FTransform NewTransform = UKismetMathLibrary::MakeTransform(
			NewLocation,
			FRotator::ZeroRotator,
			L_Scale
		);
		GetWorld()->SpawnActor<ABonusSpeed>(BonusSpeedClass,NewTransform);
		GetWorldTimerManager().SetTimer(FoodHandle, this, &ASpawnFood::OnTimerFood, 20.0f, true, 20.0f);
	}

	for (int i = 0; i <= Elements; ++i)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
		FVector L_Scale = FVector(2, 2, 2);
		FVector NewLocation = GetRandomFoodPos();
		FTransform NewTransform = UKismetMathLibrary::MakeTransform(
			NewLocation,
			FRotator(0, 90, 0),
			L_Scale
		);
		GetWorld()->SpawnActor<ABonusStar>(BonusStarClass, NewTransform);
		GetWorldTimerManager().SetTimer(FoodHandle, this, &ASpawnFood::OnTimerFood, FMath::RandRange(5, 25), true, 20.0f);
	}

	

}