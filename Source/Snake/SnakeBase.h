// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include"BlockAndDestroy.h"
#include "SnakeBase.generated.h"


class ASnakeElementBase;
//class ASnakeElementBase;
//UENUM()
UENUM(BlueprintType)
 enum class EMovementDirection : uint8
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase>SnakeElementClass;

	UPROPERTY(editDefaultsOnly)
	float ElementSize;

	UPROPERTY(editDefaultsOnly)
	float MovementSpeed;

	UPROPERTY()
	TArray<ASnakeElementBase*>SnakeElements;

	UPROPERTY(BlueprintReadOnly)
	EMovementDirection LastMoveDirection;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABlockAndDestroy>BlockAndDestroyClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FVector>BlockDestroyLocation;

	FVector PrevLocation;
	FVector FoodLocations;
	FVector LocationHead;
	FVector SnakeLocHead;
	UPROPERTY(EditDefaultsOnly)
		int x = 5;
	UPROPERTY(EditDefaultsOnly)
		int y = 5;

	UPROPERTY(EditDefaultsOnly)
		int Padding = 450;

	float X;
	float Y;
	float Z;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum=1);

	FVector GetLocationSnakeHead();

	void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement,AActor*Other);

};
