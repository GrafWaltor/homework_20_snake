// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "GameFramework/GameModeBase.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundBase.h"
#include "SnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_API ASnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	ASnakeGameModeBase();

	void BeginPlay() override;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
		//class USoundBase* GameOverSoun;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int SnakeScore;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UAudioComponent* GameOverSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int BonusScore;

	int Score = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int StarScore = 200;


	void SetScore();
	void SetBonusScore();

	UUserWidget* GetGameOverWidget();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<UUserWidget> GameOverWidgetClass;

	UUserWidget* GameOverWidget;

	private:

	
	


	
};
