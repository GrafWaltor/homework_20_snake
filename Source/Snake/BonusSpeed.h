// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BlockAndDestroy.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeBase.h"
#include "Components/SphereComponent.h"
#include "BonusSpeed.generated.h"

UCLASS()
class SNAKE_API ABonusSpeed : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusSpeed();
	UPROPERTY(EditAnywhere)
	float MovementSpeedMax;

	UPROPERTY(EditAnywhere)
	float MovementSpeedMin;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonusSpeed>BonusSpeedClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABlockAndDestroy>BlockAndDestroyClass;
	
	//UPROPERTY(EditAnywhere)
	//USphereComponent*CollisionComponent;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

    UFUNCTION()
	void OnTimerFinished();

	FTimerHandle BonusHandle;
	
	ASnakeBase* SavedSnake;
	
	
};
