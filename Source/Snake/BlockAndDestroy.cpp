// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockAndDestroy.h"
#include "SnakeBase.h"
#include "SnakeGameModeBase.h"
#include "Blueprint/UserWidget.h"

// Sets default values
ABlockAndDestroy::ABlockAndDestroy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABlockAndDestroy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlockAndDestroy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockAndDestroy::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
		ASnakeGameModeBase* gameMode = GetWorld()->GetAuthGameMode<ASnakeGameModeBase>();
		gameMode->GetGameOverWidget()->SetVisibility(ESlateVisibility::Visible);
		auto PlayerController=GetWorld()->GetFirstPlayerController();
		FInputModeUIOnly InputModeData;
		PlayerController->SetInputMode(InputModeData);
		PlayerController->bShowMouseCursor = true;
		
	}
}
