// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include"SnakeBase.h"
#include"Components/InputComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;


}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	//SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	//Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAction("LeftMouseButton",IE_Pressed,this,&APlayerPawnBase::LeftMouseButton);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

/*void APlayerPawnBase::SetVelocityDirection(const EMovementDirection direction)
{
	FVector directionVec;
	switch (direction)
	{
	case EMovementDirection::DOWN:
		directionVec = FVector(0, 1, 0);
		break;
	case EMovementDirection::UP:
		directionVec = FVector(0, -1, 0);
		break;
	case EMovementDirection::LEFT:
		directionVec = FVector(1, 0, 0);
		break;
	case EMovementDirection::RIGHT:
		directionVec = FVector(-1, 0, 0);
		break;
	}

	BulletActor->BulletSnakeProjectileMovement->SetVelocityInLocalSpace(directionVec*Speed);
}
*/

void APlayerPawnBase::HandlePlayerVerticalInput(float Value)
{
	if (IsValid(SnakeActor))
	{
		//GEngine->AddOnScreenDebugMessage(112233, 15.0f, FColor::Yellow, FString::Printf(TEXT("Vert value = %f"), Value));
		if (Value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (Value<0 && SnakeActor->LastMoveDirection !=EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
		
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float Value)
{
	if (IsValid(SnakeActor))
	{
		//GEngine->AddOnScreenDebugMessage(112244, 15.0f, FColor::Yellow, FString::Printf(TEXT("Hor value = %f"), Value));
		if (Value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if (Value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}

	}
}

void APlayerPawnBase::LeftMouseButton()
{

		//GEngine->AddOnScreenDebugMessage(112244, 15.0f, FColor::Red, FString::Printf(TEXT("I'm Pressing Action")));
		FVector L_Scale = FVector(1, 1, 1);
		FVector NewLocation =SnakeActor->GetLocationSnakeHead();
		FTransform NewTransform = UKismetMathLibrary::MakeTransform(
			NewLocation,
			FRotator::ZeroRotator,
			L_Scale
		);
		ABulletSnake*Bullet=GetWorld()->SpawnActor<ABulletSnake>(BulletSnakeClass, NewTransform);
		Bullet->SetVelocityDirection(SnakeActor->LastMoveDirection);
		



}

