// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameModeBase.h"
#include "Food.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"


	ASnakeGameModeBase::ASnakeGameModeBase()
	{
		PrimaryActorTick.bCanEverTick = true;
		GameOverSound = CreateDefaultSubobject<UAudioComponent>(TEXT("GameOverSound")); 
		GameOverSound->SetupAttachment(RootComponent);
		GameOverSound->SetPaused(true);

	}
	void ASnakeGameModeBase::BeginPlay()
	{
		Super::BeginPlay();
		GameOverWidget = CreateWidget(GetWorld(), GameOverWidgetClass);
		GameOverWidget->AddToViewport(0);
		GameOverWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	
	
	void ASnakeGameModeBase::SetScore()
	{
		SnakeScore = Score++;
		
	}

	void ASnakeGameModeBase::SetBonusScore()
	{
		BonusScore = BonusScore + StarScore;


	}


	UUserWidget* ASnakeGameModeBase::GetGameOverWidget()
	{
		//FVector SoundLoc = FVector(0, 0, 0);
		//UGameplayStatics::SpawnSoundAtLocation(this, GameOverSound, SoundLoc);
		GameOverSound->SetPaused(false);
		return GameOverWidget;
	}
	




