﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "StopBonus.h"

#include "SnakeBase.h"
#include "TimerManager.h"
#include "Engine/World.h"


// Sets default values
AStopBonus::AStopBonus()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AStopBonus::BeginPlay()
{
	Super::BeginPlay();
	
}
void AStopBonus::OnTimerFinish()
{
	SavedBonus->MovementSpeed=MovementSpeedMin;
	Destroy();
}
void AStopBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			SavedBonus=Snake;
			Snake->MovementSpeed=MovementSpeedMax;
			//GetWorldTimerManager().SetTimer(BonusStopHandle,this,&AStopBonus::OnTimerFinish,5.0f, false, 5.0f);
			//FVector NewLocation(FMath::RandRange(-1000,1000), FMath::RandRange(-1000,1000), 0);
			//FTransform NewTransform(NewLocation);
			//GetWorld()->SpawnActor<AStopBonus>(BonusStopClass,NewTransform);
			SetActorEnableCollision(false);
			GetRootComponent()->SetVisibility(false);
		
		}

	}

}


// Called every frame
void AStopBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

