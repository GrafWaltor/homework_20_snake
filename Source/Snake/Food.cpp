// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Kismet/GameplayStatics.h"
#include"SpawnFood.h"
#include"SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake= Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			ASnakeGameModeBase* gameMode = GetWorld()->GetAuthGameMode<ASnakeGameModeBase>();
			gameMode->SetScore();
			

			//FVector NewLocation(FMath::RandRange(-500, 500), FMath::RandRange(-500, 500), 0);
			//FTransform NewTransform(NewLocation);
			//GetWorld()->SpawnActor<AFood>(FoodClass_Apple,NewTransform);
			//GetWorld()->SpawnActor<AFood>(FoodClass_Banan,NewTransform);
			//GetWorld()->SpawnActor<AFood>(FoodClass_Orange,NewTransform);
			Destroy();
		}
		

	}
	

}

