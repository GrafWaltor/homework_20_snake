// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "SnakeBase.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundBase.h"
#include "BulletSnake.generated.h"



UCLASS()
class SNAKE_API ABulletSnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABulletSnake();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UAudioComponent* BulletSound = nullptr;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
		//class USoundBase* Bullet;




	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* BulletSnake = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UProjectileMovementComponent* BulletSnakeProjectileMovement = nullptr;

	//UFUNCTION()
	//void SetVelocity();

	UFUNCTION(BlueprintCallable)
		 void SetVelocityDirection(const EMovementDirection direction);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		 float Speed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FVector Velocity;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
