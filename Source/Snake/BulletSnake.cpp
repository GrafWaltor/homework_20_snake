// Fill out your copyright notice in the Description page of Project Settings.

#include "BulletSnake.h"
#include "Snake/PlayerPawnBase.h"

// Sets default values
ABulletSnake::ABulletSnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	BulletSnake = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Mesh"));
	BulletSnake->SetupAttachment(RootComponent);
	BulletSnake->SetCanEverAffectNavigation(false);
	
	BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("GameOverSound"));
	BulletSound->SetupAttachment(RootComponent);
	
	BulletSnakeProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletSnakeProjectileMovement->UpdatedComponent = RootComponent;
	BulletSnakeProjectileMovement->InitialSpeed = 1.f;
	BulletSnakeProjectileMovement->MaxSpeed = 0.f;

	BulletSnakeProjectileMovement->bRotationFollowsVelocity = true;
	BulletSnakeProjectileMovement->bShouldBounce = true;
	
}


// Called when the game starts or when spawned
void ABulletSnake::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ABulletSnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//SetVelocity();
}

void ABulletSnake::SetVelocityDirection(const EMovementDirection direction)
{
	FVector directionVec;
	switch (direction)
		{
	case EMovementDirection::DOWN:
		directionVec = FVector(0, 1, 0);
		break;
	case EMovementDirection::UP:
		directionVec = FVector(0, -1, 0);
		break;
	case EMovementDirection::LEFT:
		directionVec = FVector(1, 0, 0);
		break;
	case EMovementDirection::RIGHT:
		directionVec = FVector(-1, 0, 0);
		break;
		}

	BulletSnakeProjectileMovement->SetVelocityInLocalSpace(directionVec * Speed);



}


