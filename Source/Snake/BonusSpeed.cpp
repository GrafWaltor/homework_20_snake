// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeed.h"
#include "BlockAndDestroy.h"
#include"SnakeBase.h"
#include"TimerManager.h"

// Sets default values
ABonusSpeed::ABonusSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABonusSpeed::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABonusSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void ABonusSpeed::OnTimerFinished()
{
	SavedSnake->MovementSpeed=MovementSpeedMin;
	Destroy();
}
void ABonusSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			SavedSnake=Snake;
			Snake->MovementSpeed=MovementSpeedMax;
			GetWorldTimerManager().SetTimer(BonusHandle,this,&ABonusSpeed::OnTimerFinished,10.0f, true, 10.0f);
			// FVector NewLocation=GetRandomFoodPos(FMath::RandRange(-1000,1000), FMath::RandRange(-1000,1000), 0);
			// FTransform NewTransform(NewLocation);
			//GetWorld()->SpawnActor<ABonusSpeed>(BonusSpeedClass,NewTransform);
			SetActorEnableCollision(false);
			GetRootComponent()->SetVisibility(false);
		
		}

	}

}
